<?php

/**
 * Crea un script listado_usuarios.php que lea y muestre todos los usuarios de la base de datos. Debe mostrarlos
 * en una elemento <html>
 */

include __DIR__ . "/B_connection.php";

$sql = "SELECT id,firstName, lastName, phoneNumber, active, createdOn FROM User";
$listadoUsuarios = $conexion->query($sql);

echo "<table>";
echo "<tr>
            <th>id</th>
            <th>Nombre </th>
            <th>Apellidos</th>
            <th>Número de teléfono</th>
            <th>Activado</th>
            <th>Fecha de creación</th>
          </tr>";

foreach ($listadoUsuarios as $row) {
    echo "<tr>
                <th>{$row['id']}</th>
                <th>{$row['firstName']}</th>
                <th>{$row['lastName']}</th>
                <th>{$row['phoneNumber']}</th>
                <th>{$row['active']}</th>
                <th>{$row['createdOn']}</th>
              </tr>";
}

echo "<table>";