<?php

include __DIR__ . "/B_connection.php";

$sql = "INSERT INTO User (firstName,lastName,email,phoneNumber,password,locale,idEnterprise,birthday) VALUES
            ('Empar','Seguí Domenech','cliente1@empresa1.com','cambiame','111111111','ca_ES',6,'1990-01-08'),
            ('Vanesa','García Pérez','cliente2@empresa1.com','cambiame','22222222','es_ES',6,'1984-08-02'),
            ('Mónica','Fernández Fernández','cliente3@empresa1.com','cambiame','33333333','en_GB',6,'1980-08-02'),
            ('Elena','Martín Sánchez','cliente4@empresa1.com','cambiame','44444444','en_US',6,'1994-08-02')
            ";

$numAffectedRows = $conexion->exec($sql);

if($numAffectedRows > 0) {

    echo "insert correcto <br>";
    echo "Filas insertadas: " . $numAffectedRows . "<br>";

} else {

    print_r($conexion->errorInfo());

}