<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require __DIR__ . "/../resources/A_database-params.php";

try {

    $usuario = "usuario-incorrecto";
    $contraseña =  $dataBaseParams['password'];
    $ipServidor = $dataBaseParams['host'];
    $databaseName = $dataBaseParams['database'];
    $conexión = new PDO("mysql:host=$ipServidor;dbname=$databaseName", $usuario, $contraseña);

    echo "Conexion establecida ";

} catch (PDOException $e) {

    echo "Error en la conexión con la Base de datos " . $e->getMessage() . "<br/>";
    die();

}