<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Listado de Usuarios</title>
    <meta name="description" content="The HTML5 Herald">
    <meta name="author" content="SitePoint">

<?php

/**
 * Crea un script listado_empresas.php que lea y muestre todas las empresas de la base de datos. Al igual que el
 * anterior debe mostrarlo en un elemento <table>
 */

    include __DIR__ . "/B_connection.php";

    $sql = "SELECT id,name,address,city,province,country,nif FROM Enterprise";
    $listadoEmpresas = $conexion->query($sql);

    echo "<table>";
    echo "<tr>
            <th>id</th>
            <th>Nombre </th>
            <th>Dirección</th>
            <th>Ciudad</th>
            <th>Provincia</th>
            <th>País</th>
            <th>Nif</th>
         </tr>";

    foreach ($listadoEmpresas as $row) {
        echo "<tr>
                <th>{$row['id']}</th>
                <th>{$row['name']}</th>
                <th>{$row['address']}</th>
                <th>{$row['city']}</th>
                <th>{$row['province']}</th>
                <th>{$row['country']}</th>
                <th>{$row['nif']}</th>
              </tr>";
    }

    echo "<table>";

    ?>
</html>
