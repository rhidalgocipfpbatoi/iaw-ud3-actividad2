<?php

/** Crea un script elimina_usuarios.php que elimine los usuarios cuyo email termine en test.com */

include __DIR__ . "/B_connection.php";

$sql = "DELETE FROM User WHERE email LIKE '%test.com'";

$numAffectedRows = $conexion->exec($sql);

//Comprobamos errorCode porque podría darse el caso de que no afectará a ninguna fila
if($conexion->errorCode() !== false) {

    echo "Usuarios eliminados: " . $numAffectedRows . "<br>";

} else {

    print_r($conexion->errorInfo());

}