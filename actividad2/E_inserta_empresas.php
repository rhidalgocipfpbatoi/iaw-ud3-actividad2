<?php

include __DIR__ . "/B_connection.php";

$sql = "INSERT INTO Enterprise (name,address,city,province,country,locale,nif) VALUES
            ('Empresa1','C/ Alicante, 1','Alcoi','Alicante','ES','es_ES','21543341R'),
            ('Empresa2','C/ Alicante, 1','Cocentaina','Alicante','ES','es_ES','11543341R'),
            ('Empresa3','C/ La Rambla, 3','Ontinyent','Valencia','ES','es_ES','41543341R')";

$numAffectedRows = $conexion->exec($sql);

if($numAffectedRows > 0) {

    echo "insert correcto <br>";
    echo "Filas insertadas: " . $numAffectedRows . "<br>";

} else {

    print_r($conexion->errorInfo());

}