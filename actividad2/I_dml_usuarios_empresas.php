<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>DML Usuarios</title>
</head>
<body>
<?php

include __DIR__ . "/B_connection.php";

/**
 * Crear una tabla html <table> con los 5 primeros usuarios de la Empresa1 ordenados por el campo id.
 */

$sql = "SELECT id, firstName, lastName, phoneNumber, active, createdOn FROM User WHERE idEnterprise=6 ORDER BY id LIMIT 5";
$listadoUsuarios = $conexion->query($sql);

echo "<h2>5 primeros usuarios de la empresa 'Empresa 1'</h2>";
echo "<table>";
echo "<tr>
            <th>id</th>
            <th>Nombre </th>
            <th>Apellidos</th>
            <th>Número de teléfono</th>
            <th>Activado</th>
            <th>Fecha de creación</th>
          </tr>";

foreach ($listadoUsuarios as $row) {
    echo "<tr>
             <td>{$row['id']}</td>
             <td>{$row['firstName']}</td>
             <td>{$row['lastName']}</td>
             <td>{$row['phoneNumber']}</td>
             <td>{$row['active']}</td>
             <td>{$row['createdOn']}</td>
         </tr>";
}

echo "</table>";

/**
 * Mostrar una tabla html <table> con todas las empresas de la provincia de Alicante.
 */
$sql = "SELECT id,name,address,city,province,country,nif FROM Enterprise WHERE province = 'Alicante'";
$listadoEmpresas = $conexion->query($sql);

echo "<h2>Empresas de la provincia de Alicante</h2>";
echo "<table>";
echo "<tr>
          <th>id</th>
          <th>Nombre </th>
          <th>Dirección</th>
          <th>Ciudad</th>
          <th>Provincia</th>
          <th>País</th>
          <th>Nif</th>
       </tr>";

foreach ($listadoEmpresas as $row) {
    echo "<tr>
              <td>{$row['id']}</td>
              <td>{$row['name']}</td>
              <td>{$row['address']}</td>
              <td>{$row['city']}</td>
              <td>{$row['province']}</td>
              <td>{$row['country']}</td>
              <td>{$row['nif']}</td>
          </tr>";
}

echo "<table>";

/**
 * Insertar 10 usuarios generados de forma aleatoria a partir de la combinación de datos presentes en el siguiente array.
 */

echo "<h2>Inserción 10 usuarios aleatorios</h2>";

$informacionBase = [
    "firstName" => ["Rafa", "Alejandro", "Bea", "Joaquin", "Oscar", "Alvaro", "Sergio", "Matias"],
    "lastName" => ["Máximo","Cervera","Finalet","Herrero","Gilaber","Avellán","Llorca","Lucas","Millan"],
    "locale" => ["ca_ES", "es_ES", "en_GB"]
];

for ($i = 0; $i < 10; $i++) {

    $firstName = $informacionBase['firstName'][mt_rand(0,count($informacionBase['firstName'])-1)];
    $lastName1 = $informacionBase['lastName'][mt_rand(0,count($informacionBase['lastName'])-1)];
    $lastName2 = $informacionBase['lastName'][mt_rand(0,count($informacionBase['lastName'])-1)];
    $completeLastName = $lastName1 . " " . $lastName2;
    $locale =  $informacionBase['locale'][mt_rand(0,count($informacionBase['locale'])-1)];
    $emailPrefix = substr($firstName,0,2) . substr($lastName1, 0,2) . substr($lastName2, 0,2);
    $email = strtolower($emailPrefix).rand(0,100)."@empresa1.com";
    $password = password_hash("cambiame", PASSWORD_BCRYPT);
    $phoneNumber = mt_rand(4000, 5000);
    $birthday = "1981-11-".mt_rand(1,30);

    echo "<lu>Se va a insertar el usuario:</li>";
    echo "<li>FirstName : $firstName</li>";
    echo "<li>LastName : $completeLastName</li>";
    echo "<li>Locale : $locale</li>";
    echo "<li>Email : $email</li>";
    echo "<li>Password : $password</li>";
    echo "<li>PhoneNumber : $phoneNumber</li>";
    echo "<li>Birthday : $birthday</li>";
    echo "</lu>";

    $sql = "INSERT INTO User (firstName,lastName,email,phoneNumber,password,locale,idEnterprise,birthday) VALUES
            ('$firstName','$completeLastName','$email','$phoneNumber','$password','$locale',6,'$birthday')";

    $affectedRows = $conexion->exec($sql);

    if ($affectedRows) {
        echo "<p>Usuario insertado</p>";
    } else {
        echo "<p>Se ha producido un error al insertar el usuario</p>";
        print_r($conexion->errorInfo());
    }
    echo "<br>";

}
?>
</body>
</html>
