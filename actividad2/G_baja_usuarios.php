<?php

/** Crea un script baja_usuarios.php que desactive todos los usuarios de la Empresa1 */

include __DIR__ . "/B_connection.php";

$sql = "UPDATE User SET active = 0 WHERE idEnterprise = 6";

$numAffectedRows = $conexion->exec($sql);

//Comprobamos errorCode porque podría darse el caso de que no afectará a ninguna fila
if($conexion->errorCode() !== false) {

    echo "Usuarios desactivadas: " . $numAffectedRows . "<br>";

} else {

    print_r($conexion->errorInfo());

}