<?php

/**
 * A - Declara una función que devuelva un array con todos los usuarios que pertenezcan a la empresa pasada como argumento.
 *
 * B - Crea una funcion que reciba como parámetro el id del usuario a buscar y devuelva un array con su información.
 *
 * C - Crea una funcion llamada modificarUsuario que reciba como parámetro un array con los datos del usuario a modificar
 *    devolverá true/false en función de si la acción se ha llevado a cabo correctamente o no.
 *
 * D - Crea una funcion que reciba como parámetro el id del usuario a borrar y devuelva `true/false`
 *    en función de si la acción se ha llevado a cabo correctamente o no.
 **/

include __DIR__ . "/A_connection_pdo.php";

/**
 * Obtiene el listado de todos los usuarios perteneciente a la empresa
 *
 * @param int $empresaId
 * @return array
 */
function obtenerUsuarios(int $empresaId) : array
{
    $conexion = crearConexion();
    $sql = "SELECT * FROM User WHERE idEnterprise = $empresaId";
    $listadoEmpresas = $conexion->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    return $listadoEmpresas;
}

/**
 * Busca un usuario con el $id Correspondiente
 *
 * @param int $id
 * @return array
 */
function buscarUsuario(int $id) : array
{
    $conexion = crearConexion();
    $sql = "SELECT * FROM User WHERE id = $id";
    $usuario = $conexion->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    return ($usuario) ? $usuario[0] : array();
}

/**
 * Modifica los datos de un usuarios guardados en la base de datos
 *
 * @param array $usuario [ firstName, lastName, email, active, phoneNumber, locale, birthday, idEnterprise, password]
 * @return bool
 */
function modificarUsuario(array $usuario) : bool
{
    $conexion = crearConexion();

    $sql = "UPDATE User SET 
                firstName = '{$usuario['firstName']}',
                lastName = '{$usuario['lastName']}',
                email = '{$usuario['email']}',
                phoneNumber = '{$usuario['phoneNumber']}',
                locale = '{$usuario['locale']}',
                birthday = '{$usuario['birthday']}',
                idEnterprise = {$usuario['idEnterprise']},
                active = {$usuario['active']}
                WHERE id = {$usuario['id']} ";

    $numAffectedRows = $conexion->exec($sql);

    if($numAffectedRows !== false ) {

        return true;

    } else {

        print_r($conexion->errorInfo());
        return false;

    }
}

/**
 * Borra el usuario con id correspondiente
 *
 * @param int $id
 * @return bool
 */
function borrarUsuario(int $id) : bool
{
    $conexion = crearConexion();
    $sql = "DELETE FROM User WHERE id = $id";

    $numAffectedRows = $conexion->exec($sql);

    if ($conexion->errorCode() == "00000" || $numAffectedRows > 0) {
        return true;
    } else {
        print_r($conexion->errorInfo());
        return false;
    }
}

/**
 * Inserta un usuario en la Base de datos
 *
 * @param array $usuario [firstName,lastName,email,active,phoneNumber, locale, birthday, idEnterprise, password]
 * @return array
 */
function insertarUsuario(array $usuario) : array
{
    $conexion = crearConexion();

    $sql = "INSERT INTO User (firstName,lastName,email,active,phoneNumber, locale, birthday, idEnterprise, password)
            VALUES ('{$usuario['firstName']}', '{$usuario['lastName']}', '{$usuario['email']}', 
                    '{$usuario['active']}', '{$usuario['phoneNumber']}', '{$usuario['locale']}',
                    '{$usuario['birthday']}', {$usuario['idEnterprise']}, '{$usuario['password']}')";

    $numAffectedRows = $conexion->exec($sql);

    if ( $numAffectedRows ) {

        $idAsignado = $conexion->lastInsertId();
        return buscarUsuario($idAsignado);

    } else {

        print_r($conexion->errorInfo());
        return [];

    }
}