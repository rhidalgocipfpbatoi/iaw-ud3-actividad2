<?php
function mostrarUsuarios(array $listadoUsuarios)
{

    echo "<table>";
    echo "<tr>
            <th>id</th>
            <th>Nombre </th>
            <th>Apellidos</th>
            <th>Número de teléfono</th>
            <th>Activado</th>
            <th>Fecha de creación</th>
          </tr>";

    foreach ($listadoUsuarios as $row) {
        echo "<tr>
                <th>{$row['id']}</th>
                <th>{$row['firstName']}</th>
                <th>{$row['lastName']}</th>
                <th>{$row['phoneNumber']}</th>
                <th>{$row['active']}</th>
                <th>{$row['createdOn']}</th>
              </tr>";
    }

    echo "</table>";

}

function mostrarUsuario(array $usuario)
{

    echo "<ul>
            <li>Id: {$usuario['id']}</li>
            <li>Nombre: {$usuario['firstName']}</li>
            <li>Apellidos: {$usuario['lastName']}</li>
            <li>Telefono: {$usuario['phoneNumber']}</li>
            <li>Activo: {$usuario['active']}</li>
            <li>Fecha de Creación: {$usuario['createdOn']}</li>
            <li>idEnterprise: {$usuario['idEnterprise']}</li>
            <li>Cumpleaños: {$usuario['birthday']}</li>
         </ul>";

}

require __DIR__ . "/B_repositorio_usuario_pdo.php";

echo "<h2>Obtenemos y mostramos todos los usuarios</h2>";
$idEmpresa = 6;
$listadoUsuarios = obtenerUsuarios($idEmpresa);
mostrarUsuarios($listadoUsuarios);

if (count($listadoUsuarios) > 0) {
    $usuarioSeleccionado = $listadoUsuarios[0];
    echo "<h2>El id del usuario seleccionado es {$usuarioSeleccionado['id']}</h2>";
    mostrarUsuario($usuarioSeleccionado);
    echo "<h2>Modificamos el nombre del usuario</h2>";
    $usuarioSeleccionado['firstName'] = "Usuario Cambiado";
    modificarUsuario($usuarioSeleccionado);

    $usuarioSeleccionado = buscarUsuario($usuarioSeleccionado['id']);
    mostrarUsuario($usuarioSeleccionado);

    echo "<h2>Borramos el usuario</h2>";

    if (borrarUsuario($usuarioSeleccionado['id'])) {

        echo "<p>El usuario se ha borrado</p>";

    } else {

        echo "</p>El usuario no se ha borrado</p>";
    }
} else {
    echo "<h2> No hay usuarios de la empresa $idEmpresa </h2>";
}


