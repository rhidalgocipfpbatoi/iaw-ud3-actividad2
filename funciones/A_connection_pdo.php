<?php

/**
 * Crea un script `A_connection_pdo.php` y declara una función que al referenciarla devuelva un objeto de tipo ``PDO``
 * con la conexión a la base de datos establecida
 **/
function crearConexion() : ?PDO
{
    require __DIR__ . "/../resources/A_database-params.php";

    try {

        $usuario = $dataBaseParams['user'];
        $contraseña =  $dataBaseParams['password'];
        $ipServidor = $dataBaseParams['host'];
        $databaseName = $dataBaseParams['database'];
        $conexion = new PDO("mysql:host=$ipServidor;dbname=$databaseName;charset=utf8", $usuario, $contraseña);

    } catch (PDOException $e) {

        echo "Error en la conexión con la Base de datos " . $e->getMessage() . "<br/>";
        return null;

    }

    return $conexion;
}