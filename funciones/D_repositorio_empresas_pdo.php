<?php

/**
 * A - Declara una función que devuelva un array con todos los usuarios que pertenezcan a la empresa pasada como argumento.
 *
 * B - Crea una funcion que reciba como parámetro el id del usuario a buscar y devuelva un array con su información.
 *
 * C - Crea una funcion llamada modificarUsuario que reciba como parámetro un array con los datos del usuario a modificar
 *    devolverá true/false en función de si la acción se ha llevado a cabo correctamente o no.
 *
 * D - Crea una funcion que reciba como parámetro el id del usuario a borrar y devuelva `true/false`
 *    en función de si la acción se ha llevado a cabo correctamente o no.
 **/

require_once __DIR__ . "/A_connection_pdo.php";

/**
 * Obtiene el listado de todos las empresas
 *
 * @param int $empresaId
 * @return array
 */
function findAll(int $page, String $querString = "", int $pageSize = 15) : array
{
    $conexion = crearConexion();

    $offset = $page * $pageSize;

    if ($querString) {

        $sql = "SELECT * FROM Enterprise WHERE name LIKE '%$querString%' LIMIT $offset, $pageSize";

    } else {

        $sql = "SELECT * FROM Enterprise LIMIT $offset, $pageSize";

    }

    $listadoEmpresas = $conexion->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    return $listadoEmpresas;
}

/**
 * Busca una empresa con el $id Correspondiente
 *
 * @param int $id
 * @return array
 */
function findEnterprise(int $id) : array
{
    $conexion = crearConexion();

    $sql = "SELECT * FROM Enterprise WHERE id = $id";
    $enterprise = $conexion->query($sql)->fetchAll(PDO::FETCH_ASSOC);

    return ($enterprise) ? $enterprise[0] : array();
}

/**
 * Inserta un empresa en la Base de datos
 *
 * @param array $enterprise [name,nif,address,city,province,country,locale,status]
 * @return array
 */
function insertEnterprise(array $enterprise) : array
{
    $conexion = crearConexion();

    $sql = "INSERT INTO Enterprise (name,nif,address,city,province,country,locale,status)
            VALUES ('{$enterprise['name']}', '{$enterprise['nif']}', '{$enterprise['address']}', 
                    '{$enterprise['city']}', '{$enterprise['province']}', '{$enterprise['country']}',
                    '{$enterprise['locale']}', {$enterprise['status']})";

    $numAffectedRows = $conexion->exec($sql);

    if( $numAffectedRows > 0 ) {

        return findEnterprise($conexion->lastInsertId());

    } else {

        print_r($conexion->errorInfo());
        return [];

    }
}

/**
 * Modifica los datos de una empresa
 *
 * @param array $enterprise [name,nif,address,city,province,country,locale,status]
 * @return bool
 */
function updateEnterprise(array $enterprise) : bool
{
    $conexion = crearConexion();

    $sql = "UPDATE Enterprise SET 
                name = '{$enterprise['name']}',
                nif = '{$enterprise['nif']}',
                address = '{$enterprise['address']}',
                city = '{$enterprise['city']}',
                province = '{$enterprise['province']}',
                country = '{$enterprise['country']}',
                locale = '{$enterprise['locale']}',
                status = {$enterprise['status']}
                WHERE id = {$enterprise['id']} ";

    $numAffectedRows = $conexion->exec($sql);

    if($numAffectedRows !== false ) {
        return true;
    } else {
        print_r($conexion->errorInfo());
        return false;
    }
}

/**
 * Borra una empresa
 *
 * @param int $id
 * @return bool
 */
function deleteEnterprise(int $id) : bool
{
    $conexion = crearConexion();
    $sql = "DELETE FROM Enterprise WHERE id = $id";

    $numAffectedRows = $conexion->exec($sql);

    if ($numAffectedRows !== false) {
        return true;
    } else {
        print_r($conexion->errorInfo());
        return false;
    }
}