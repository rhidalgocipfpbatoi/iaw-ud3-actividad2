﻿|![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.001.png)|![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.002.png)|![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.003.png)|![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.004.png)|![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.005.png)|
| :- | :- | :- | :- | :- |

**Actividad semi-resuelta 3.2** 

**Acceso a Base de Datos con PDO’s (PHP Data Objects)**

1. ### **Preparando el entorno**
Para realizar esta práctica guiada habrá que **instalarse antes XAMPP**. La instalación la puedes realizar a través de la página  <https://www.apachefriends.org/es/index.html> 

**Ejecuta el manager** (/opt/lampp/manager-linux-x64.run) y levanta los servicios Apache y MySQL a través del manager de la aplicación.

**IMPORTANTE:** Si tienes problemas para levantar el servicio MySQL, asegúrate de que en tu máquina no hay un servicio MySQL ya levantado, de otro modo, el manager no permitirá levantar el servicio.

1. ### **Importación de la base de datos** 
El primer paso será la importación del ***schema*** de la base de datos que se va a utilizar en la práctica. El fichero de importación lo puedes obtener a través del enlace disponible en aules (crm\_db.sql). Descomprime el fichero zip obtenido y extrae el fichero .sql dentro de él.

Si abres este fichero con extensión .sql, verás que no contiene como primera directiva la creación de la base de datos a importar, por lo que deberás crear primero la base de datos antes de importar el archivo **crm\_db.sql**. 

Para realizar esta acción tenemos dos opciones:

1. Modificar el fichero .sql añadiendo la instrucción sql que nos va a permitir crear la base de datos antes de proceder con la importación.
1. Crear a mano antes la base de datos para realizar la importación.

Si optas por la opción 1, deberás escribir al principio del script sql la instrucción:

CREATE DATABASE IF NOT EXISTS `crm\_db`;

USE `crm\_db`;

Si optas por la opción 2, realizaremos la acción a través de la herramienta *phpMyAdmin*, a la cual podrás acceder escribiendo en la barra de direcciones de tu navegador la url ***http://localhost/phpmyadmin***

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.006.png)

Fíjate que tienes un cuadro de texto que te permite escribir el nombre de una nueva base de datos. Escribe en este caso el texto *crm\_db* y pulsa en el botón “**Crear**” (no es necesario que modifiques el tipo de codificación, es decir, el desplegable de la izquierda del cuadro).

Tanto si has seguido la opción 1 como la opción 2, nos quedará importar la base de datos (el esquema y los datos) para tenerla disponible para la práctica. Para ello, accederemos a la pestaña “**Importar**”.

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.007.png)

Seleccionamos el archivo .sql y pulsamos en el botón “Importar” al final de la pantalla. Si todo ha ido bien, deberás ver una lista de acciones aprobadas (con el check verde de que todo ha ido bien). Si aparece algún mensaje con la marca roja, es que algo ha ido mal y deberías revisar todos los pasos anteriores. 

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.008.png)
1. ### **Creación del usuario de conexión** 
Una vez importada la base de datos ***crearemos un usuario*** para nuestra aplicación php, debemos tener en cuenta que:

- Debe ser un usuario con los mínimos privilegios que requiera nuestra aplicación para que, si hubiera vulnerabilidad, el daño fuera mínimo.
- No debemos compartir usuarios con distintas aplicaciones, en otro caso, estaríamos introduciendo acoplamientos y vulnerabilidades entre aplicaciones.
- El usuario que creemos solo tendrá permisos de acceso desde el host o la IP en la que se vaya a ejecutar nuestra aplicación. En nuestro caso localhost.

De nuevo, para realizar esta acción, podemos recurrir a hacerlo a través de instrucciones SQL, o bien a través de la propia herramienta. Lo que es necesario, en cualquier caso, es acceder a la base de datos creada, para ello, pulsa en la base de datos “crm\_db” creada. Deberías ver algo así:

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.009.png)

**Opción 1. Creación del usuario a través de una instrucción SQL**

Accedemos a la pestaña SQL y escribimos la siguiente instrucción (cambiando *rhidalgo-web* por el nombre de usuario que decidas):

![ref1]![ref2]![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.012.png)

El host del usuario no debe ser % sino localhost:



Al pulsar en continuar, deberías ver el siguiente mensaje (indicando que el usuario y sus privilegios han sido creados).


![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.013.png)

**Opción 2. Creación a través del menú de creación de usuarios de phpMyAdmin**

Accediendo al menú principal de phpMyAdmin, en concreto a la pestaña “Cuentas de usuario”, verás el siguiente listado, que corresponde con los usuarios actuales disponibles en tu servidor local:

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.014.png)

Para crear un nuevo usuario, pulsa en el enlace “Agregar cuenta de usuario”.

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.015.png)


Introduce los siguientes datos:

- Nombre de usuario: el nombre que decidas
- Nombre del Host: localhost
- Contraseña: 123456789
- Seleccionar check: Privilegios globales -> Seleccionar todo.

Pulsa en *Continuar*.

1. ### **Instalación del driver nativo php-mysql** 

El siguiente paso que llevaremos a cabo es la instalación del driver de mysql para php. Para ello, **accede a tu terminal** **local** y escribe la siguiente instrucción:

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.016.png)

***Nota**: no olvides reiniciar el servicio de Apache.*

1. ### **Codificando en php para conectar con la base de datos creada y realizar diferentes acciones desde/sobre ella.**

**Paso previo. Creación del proyecto PHP**

Abre PhpStorm con permisos de superusuario (sudo ./phpstorm.sh).

Crea un nuevo proyecto en la carpeta /opt/lampp/htdocs. Lo llamaremos *practicabd*.

**Puntos a realizar**

**A)** Crea en el proyecto un ***directorio llamado “resources”*** y crea el ***script “database-params.php”*** en el que guardaremos los parámetros de acceso a la base de datos. Rellénalo con los datos oportunos.

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.017.png)

**B)** Crea una carpeta *actividad2* y dentro de esta carpeta crea un ***script “connection.php”*** en el que:

- A partir de la inclusión del fichero anterior cree una conexión con la BD. El código sería el siguiente:

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.018.png)![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.019.png)

***Nota:** Es normal que el entorno indique que tienes un error en la variable $dataBaseParams ya que no realiza una interpretación del código y por tanto no sabe que el fichero database-params.php se declaró esa variable.*

***Nota 2:** Todos los scripts que creemos incluirán el fichero **"database-params.php"** para obtener los parámetros de conexión con la base de datos.*

**C)** Crea un ***script “listado\_usuarios.php”*** que lea y muestre todos los usuarios de la base de datos (tabla *User*). Debe mostrarlos en un elemento **<html>**

Tienes el código que ejecuta esto a continuación:

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.020.png)

**D)** Crea un ***script*** “***listado\_empresas.php”*** que lea y muestre todos los datos de las empresas de la base de datos. Similar al anterior paso, pero éste debe mostrarlo en un elemento **<table>**.

**E)** Crea un ***script “inserta\_usuarios.php”*** que dé de alta los siguientes usuarios

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.021.png)

El código que realiza esta acción lo tienes a continuación:

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.022.png)

**F)** Crea un ***script*** “***inserta\_empresas.php”*** que dé de alta las siguientes empresas

***Nota:** Deberás tener en cuenta el orden de inserción.*

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.023.png)

**G)** Crea un ***script “baja\_usuarios.php”*** que desactive todos los usuarios de la Empresa1 (campo active a 0). Para ejecutar una actualización deberás seguir usando el método exec.

**H)** Crea un ***script “elimina\_usuarios.php”*** que elimine los usuarios cuyo email termine en test.com. Para ejecutar un borrado deberás seguir usando el método exec.

**I)** Crea un ***script “dml\_usuarios\_empresas.php”*** que deberá llevar a cabo las siguientes acciones.

- Crear una tabla **html <table>** con los 5 primeros usuarios de la Empresa1 ordenados por el campo id. 
- Mostrar una tabla **html <table>** con todas las empresas de la provincia de Alicante.
- Insertar 10 usuarios generados de forma aleatoria a partir de la combinación de datos presentes en el siguiente array. 

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.024.png)

- El email vendrá dado de la concatenación de los 2 primeros caracteres del nombre y apellidos seguidos de @{nombre\_de\_la\_empresa}.com
- El password será un hash de la palabra “cámbiame” generado a partir del uso de la función [password_hash()](https://www.php.net/manual/es/function.password-hash.php)
- El teléfono vendrá dado por un número aleatorio entre 4000 y 5000.
- La fecha de nacimiento será un día aleatorio del mes de noviembre de 1981. 1981-11-{día}.

1. ### **Trabajo con funciones**

1) Crea un ***script “connection\_pdo.php”*** y declara una función que al referenciarla devuelva un objeto de tipo "PDO" con la conexión a la base de datos establecida. Nos basaremos en el código del script ***connection.php***, creado en el apartado A) del punto anterior. 	

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.025.png)Fíjate que, en este caso, la variable conexión ya no será una variable incluída, sino que se retorna. Esto hará que no tengamos el error que se obtenía anteriormente en el entorno con esta variable concreta. 

1) Crea un ***script “repositorio\_usuario\_pdo.php”*** que incluya el fichero ***“connection\_pdo.php”*** y realiza las siguientes tareas:

**a)** Declara una ***función*** llamada ***"obtenerUsuarios()"*** que devuelva un array con todos los usuarios que pertenezcan al id de la empresa pasada como argumento "**$empresaId**". Al incluir el fichero **connection\_pdo.php**, tenemos por tanto acceso a la función **crearConexion**() que nos devuelve el objeto $conexion.

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.026.png)

**b)** Crea una ***función*** llamada “***buscarUsuario”*** que reciba como parámetro el id del usuario a buscar y devuelva un array con toda la información del usuario. Si no se encuentra la empresa devolverá un array vacío.

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.027.png)

**c)** Crea una ***función*** llamada “***modificarUsuario”*** que reciba como parámetro un array con los datos del usuario a modificar. Devolverá 'true/false' en función de si la acción se ha llevado a cabo correctamente o no.

Los campos modificables son: 'firstName, lastName, email, active, phoneNumber, locale, birthday, idEnterprise, password'

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.028.png)

**d)** Crea una ***función*** llamada “***borrarUsuario”*** que reciba como parámetro el id del usuario a borrar y devuelva ‘true/false’ en función de si la acción se ha llevado a cabo correctamente o no.

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.029.png)

**e)** Crea una ***función*** llamada “***insertarUsuario”*** que reciba como parámetro un array con la información del usuario y devuelva el nuevo usuario insertado.

Los campos requeridos son: 'firstName, lastName, email, active, phoneNumber, locale, birthday, idEnterprise, password'

![](Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.030.png)

C) Crea un nuevo ***fichero `actividad2.php`*** en el que hagas uso de cada una de las funciones generadas en el apartado anterior para ello deberás:

- Mostrar todo el listado de usuarios de la empresa con id = 6
- Buscar un usuario y mostrarlo
- Modificar un usuario y mostrarlo tras su modificación.
- Borrar dicho usuario y comprobar que el borrado se ha llevado a cabo correctamente.

D) Crea un ***script “repositorio\_empresas\_pdo.php”*** y repite las mismas acciones del ejercicio anterior, pero en este caso para la entidad ‘Enterprise’ de la base de datos 'crm\_db'.
*CIPFP Batoi		9**

[ref1]: Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.010.png
[ref2]: Aspose.Words.f3d7f9f7-17a5-4afa-821a-ea95c1bf15de.011.png
